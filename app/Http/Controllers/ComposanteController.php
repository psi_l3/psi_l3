<?php

namespace App\Http\Controllers;

use App\Models\Composante;
use Illuminate\Http\Request;

class ComposanteController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('composanteExists')->only(
            'show',
            'edit',
            'update',
            'destroy'
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $composantes = Composante::getAll();

        return view('composante.index', compact( 'composantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('composante.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'libelle_composante' => 'required',
        ]);

        Composante::store($data);

        return redirect()->route('composante.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $composante = Composante::get($id);
        return view('composante.show', compact('composante'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $composante = Composante::getWithFormations($id);
        return view('composante.edit', compact('id', 'composante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'libelle_composante' => 'required',
        ]);

        Composante::update($id, $data);

        return redirect()->route('composante.edit', ['composante' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Composante::delete($id);
        return redirect()->route('composante.index');
    }

    public function composante_404(){
        return view('composante.404');
    }
}
