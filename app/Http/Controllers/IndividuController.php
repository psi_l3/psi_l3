<?php

namespace App\Http\Controllers;

use App\Models\Individu;
use App\Models\TypeIndividu;
use App\Models\IndividuTypeIndividu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Export\exportData;
use Excel;
use App\Models\ImportIndividu;
use Illuminate\Support\Facades\Storage;

class IndividuController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('individuExists')->only(
            'show',
            'edit',
            'update',
            'destroy'
        );
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $individus = Individu::getAllWithTypes();
        // dd($individus);
        return view('individu.index', compact('individus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $typesIndividu = TypeIndividu::getAll();

        return view('Individu.create', compact('typesIndividu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataindividu = $request->validate([
            'id_individu' => 'required|unique:individu,id_individu',
            'arpeg' => 'nullable',
            'nom_individu' => 'nullable',
            'prenom_individu' => 'nullable',
            'mail_individu' => 'nullable|email',
            'tel_individu' => 'nullable'
        ]);

        $datatype = $request->validate([
            'fid_type_individu' => 'required|exists:type_individu,id_type_individu',
            'annee' => 'date_format:Y'
        ]);

        Individu::create($dataindividu);
        $datatype['fid_individu'] = $dataindividu['id_individu'];
        IndividuTypeIndividu::create($datatype);

        return redirect()->route('individu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $individu = Individu::get($id);
        return view('individu.show', compact('individu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $typesIndividu = TypeIndividu::getAll();
        $individu = Individu::get($id);

        return view('Individu.edit', compact('individu','typesIndividu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataindividu = $request->validate([
            'nom_individu' => 'nullable',
            'prenom_individu' => 'nullable',
            'mail_individu' => 'nullable|email',
            'tel_individu' => 'nullable'
        ]);

        $datatype = $request->validate([
            'fid_type_individu' => 'nullable|exists:type_individu,id_type_individu|unique:individu_typeindividu,fid_type_individu',
            'annee' => 'nullable|date_format:Y'
        ]);

        Individu::update($id, $dataindividu);

        if($datatype['fid_type_individu']!=null)
        {
            $datatype['fid_individu'] = $id;
            IndividuTypeIndividu::create($datatype);
        }

        return redirect()->action('IndividuController@edit' , $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Individu::delete($id);
        return redirect()->route('individu.index');
    }


    public function individu_404(){
        return view('individu.404');
    }

    public function export(){
      return Excel::download(new exportData, 'exportEtudiants.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        return view('import');
    }

    public function importFromExcel(){
      Excel::import(new ImportIndividu,request()->file('file'));
      return back();
  }

}
