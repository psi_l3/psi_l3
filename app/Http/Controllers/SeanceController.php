<?php

namespace App\Http\Controllers;

use App\Models\Individu;
use App\Models\Groupe;
use App\Models\Salle;
use App\Models\Seance;
use App\Models\SeanceGroupe;
use App\Models\TypeSeance;
use Illuminate\Http\Request;

class SeanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $seances = Seance::getAllWithGroupe();
        return view('seance.index', compact('seances'));
    }

    public function create()
    {
        $groupes = Groupe::getAll();
        $typeSeances = TypeSeance::getAll();
        $enseignants = Individu::getEnseignants();
        $salles = Salle::getAll();

        return view('seance.create', compact(
            'groupes',
            'typeSeances',
            'enseignants',
            'salles'
        ));
    }

    public function store(Request $request)
    {
        $validatedSeanceData = $request->validate([
            'fid_type_seance' => 'nullable|exists:type_seance,id_type_seance',
            'fid_individu' => 'nullable|exists:individu,id_individu',
            'fid_salle' => 'nullable|exists:salle,id_salle',
            'date_debut_seance' => 'nullable|date',
            'date_fin_seance' => 'nullable|date',
        ]);

        $validatedGroupeData = $request->validate([
            'fid_groupe' => 'required|exists:groupe,id_groupe',
        ]);

        $idSeance = Seance::create($validatedSeanceData);
        $validatedGroupeData['fid_seance'] = $idSeance;
        SeanceGroupe::create($validatedGroupeData);

        return redirect()->action('SeanceController@index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $groupes = Groupe::getAll();
        $typeSeances = TypeSeance::getAll();
        $enseignants = Individu::getEnseignants();
        $salles = Salle::getAll();

        $seance = Seance::getWithGroupe($id);

        return view('seance.edit', compact(
            'groupes',
            'typeSeances',
            'enseignants',
            'salles',
            'seance'
        ));
    }

    public function update(Request $request, $id)
    {
        $validatedSeanceData = $request->validate([
            'fid_type_seance' => 'nullable|exists:type_seance,id_type_seance',
            'fid_individu' => 'nullable|exists:individu,id_individu',
            'fid_salle' => 'nullable|exists:salle,id_salle',
            'date_debut_seance' => 'nullable|date',
            'date_fin_seance' => 'nullable|date',
        ]);

        $validatedGroupeData = $request->validate([
            'fid_groupe' => 'required|exists:groupe,id_groupe',
        ]);

        Seance::update($id, $validatedSeanceData);
        SeanceGroupe::delete($id);
        $validatedGroupeData['fid_seance'] = $id;
        SeanceGroupe::create($validatedGroupeData);

        return redirect()->action('SeanceController@edit' , $id);
    }

    public function destroy($id)
    {
        Seance::delete($id);
        return redirect()->action('SeanceController@index');
    }
}
