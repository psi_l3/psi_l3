<?php

namespace App\Http\Controllers;

use App\Export\exportGroupe;
use App\Export\exportGroupeAll;
use App\Models\Formation;
use App\Models\Groupe;
use App\Models\Individu;
use App\Models\Modalite;
use App\Models\Niveau;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class GroupeController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function exportGroupe(){
        return Excel::download(new exportGroupe, 'exportGroupe-'.request('libelleGroupeChoix').'.xlsx');
    }

    public function exportGroupeAllData(){
        return Excel::download(new exportGroupeAll, 'exportGroupe.xlsx');
    }

    /* Valide les données pour la création ou mise à jour d'un groupe */
    public function validateGroupeData($req){

        $validatedGroupeData = $req->validate([
            'libelle_groupe' => 'required',
            'fid_niveau' => 'nullable|exists:niveau,id_niveau',
            'fid_modalite' => 'nullable|exists:modalite,id_modalite',
            'annee' => 'date_format:Y'
        ]);

        /* Validation des des données de Formation */
        $formation = $req->input('formation');
        if($formation != null){
            $formation = json_decode($formation);

            /**
            * Vérification du bon format de la donnée et de l'existence
            * de la formation dans la base de données.
            */
            if($formation == null || !Formation::exists($formation->fid_formation, $formation->f_vet)){
                throw ValidationException::withMessages(['formation' => "La formation n'existe pas."]);

            }

            $validatedGroupeData['fid_formation'] = $formation->fid_formation;
            $validatedGroupeData['f_vet'] = $formation->f_vet;
        }

        return $validatedGroupeData;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupes = Groupe::getAll();
        return view('groupe.index', compact('groupes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $niveaux = Niveau::getAll();
        $formations = Formation::getAll();
        $modalites = Modalite::getAll();

        return view('groupe.create', compact(
            'niveaux',
            'formations',
            'modalites'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedGroupeData = $this->validateGroupeData($request);

        Groupe::create($validatedGroupeData);

        return redirect()->action('GroupeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $groupe = Groupe::get($id);
        $niveaux = Niveau::getAll();
        $formations = Formation::getAll();
        $modalites = Modalite::getAll();
        $groupeEtudiants = Groupe::getEtudiants($id);
        $allEtudiants = Individu::getEtudiants();

        return view('groupe.edit', compact(
            'groupe',
            'niveaux',
            'formations',
            'modalites',
            'groupeEtudiants',
            'allEtudiants'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Groupe::exists($id)){
            return redirect()->action('GroupeController@index');
        }
        $validatedGroupeData = $this->validateGroupeData($request);
        Groupe::update($id, $validatedGroupeData);

        return redirect()->action('GroupeController@edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Groupe::exists($id)){
            Groupe::delete($id);
        }
        return redirect()->action('GroupeController@index');
    }
}
