<?php

namespace App\Http\Controllers;

use App\Models\Groupe;
use App\Models\Seance;
use App\Models\Individu;
use Illuminate\Http\Request;

class PlanningController extends Controller
{

    /* Retourne la couleur de fond d'une séance en fonction de son type */
    public function setColor($typeSeance){
        $color = "";
        if($typeSeance == "TD"){
            $color = "#99FF66";
        }
        elseif ($typeSeance == "EXAM") {
            $color = "#F89C0E";
        }
        elseif ($typeSeance == "VISIO") {
            $color = "#FFFFCC";
        }
        else {
            $color = "#3788D8";
        }
        return $color;
    }

    /*
    * Retourne les informations des séances en JSON.
    */
    public function getJsonData($seances){
        $jsonData = [];
        for ($i=0; $i < count($seances); $i++) {
            $jsonData[$i]['title'] = $seances[$i]->libelle_groupe . ' - ' . $seances[$i]->batiment . ' ' . $seances[$i]->numero_salle;
            $jsonData[$i]['start'] = $seances[$i]->date_debut_seance;
            $jsonData[$i]['end'] = $seances[$i]->date_fin_seance;
            $jsonData[$i]['groupe'] = $seances[$i]->libelle_groupe;
            $jsonData[$i]['batiment'] = $seances[$i]->batiment;
            $jsonData[$i]['salle'] = $seances[$i]->numero_salle;
            $jsonData[$i]['type_seance'] = $seances[$i]->libelle_type_seance;
            $jsonData[$i]['enseignant'] = $seances[$i]->prenom_individu . ' ' . strtoupper($seances[$i]->nom_individu);
            $jsonData[$i]['capacite'] = $seances[$i]->capacite;
            $jsonData[$i]['nb_postes'] = $seances[$i]->nb_postes;
            $jsonData[$i]['projecteur'] = $seances[$i]->projecteur;
            $jsonData[$i]['color'] = $this->setColor($seances[$i]->libelle_type_seance);
            $jsonData[$i]['textColor'] = 'black';
            if($seances[$i]->id_seance){
                $jsonData[$i]['url'] = action('SeanceController@edit', $seances[$i]->id_seance);
            }
        }

        return json_encode($jsonData);
    }

    public function index(){
        $groupes = Groupe::getAll();
        $enseignants = Individu::getEnseignants();

        return view('planning', compact(
            'groupes',
            'enseignants'
        ));
    }

    /* Retourne une réponse JSON avec les séances d'un groupe */
    public function groupePlanningJSON(){
        $idGroupe = request()->input('id_groupe');
        $seances = Groupe::getSeanceFor($idGroupe);

        $jsonData = $this->getJsonData($seances);

        return response()->json($jsonData);
    }

    /* Retourne une réponse JSON avec les séances d'un enseignant */
    public function enseignantPlanningJSON(){
        $idEnseignant = request()->input('id_enseignant');
        $seances = Individu::getSeanceFor($idEnseignant);

        $jsonData = $this->getJsonData($seances);

        return response()->json($jsonData);
    }

}
