<?php

namespace App\Http\Controllers;

use App\Models\Composante;
use App\Models\Formation;
use App\Models\FormationComposante;

use Illuminate\Http\Request;

class FormationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('formationExists')->only(
            'show',
            'edit',
            'update',
            'destroy'
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formations = Formation::getAllWithComposantes();

        return view('formation.index', compact('formations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $composantes = Composante::getAll();
        return view('formation.create', compact('composantes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $formationData = $request->validate([
            'libelle_formation' => 'required',
        ]);

        $composantesId = $request->validate([
            'id_composantes' => 'array|exists:composante,id_composante',
        ]);

        $idFormation = Formation::create($formationData);

        if(!isset($composantesId['id_composantes'])){
            $composantesId['id_composantes'] = [];
        }

        foreach ($composantesId['id_composantes'] as $composanteId) {
            FormationComposante::create($idFormation, $composanteId);
        }

        return redirect()->action('FormationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $vet)
    {
        $formation = Formation::get($id, $vet);
        return view('formation.show', compact('formation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $vet)
    {
        $formation = Formation::getWithComposantes($id, $vet);
        $isLatestVersion = Formation::isLatestVersion($id, $vet);
        $formationAllVersion = Formation::getFormationAllVersion($id);
        $composantes = Composante::getAll();
        return view('formation.edit', compact(
            'formation',
            'isLatestVersion',
            'formationAllVersion',
            'composantes'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $vet)
    {
        $dataFormation = $request->validate([
            'libelle_formation' => 'required',
        ]);

        $composantesId = $request->validate([
            'id_composantes' => 'array|exists:composante,id_composante',
        ]);

        if(!isset($composantesId['id_composantes'])){
            $composantesId['id_composantes'] = [];
        }

        $isLatestVersion = Formation::isLatestVersion($id, $vet);
        if($request->input('new_version') && $isLatestVersion){

            $newFormationData = [
                'id_formation' => $id,
                'vet' => ++$vet,
                'libelle_formation' => $dataFormation['libelle_formation'],
            ];
            $id = Formation::create($newFormationData);
            foreach ($composantesId['id_composantes'] as $composanteId) {
                FormationComposante::create($id, $composanteId, $vet);
            }
        }
        else {
            Formation::update($id, $vet, $dataFormation);
            FormationComposante::update($id, $vet, $composantesId['id_composantes']);
        }

        return redirect()->action('FormationController@edit', ['formation' => $id, 'vet' => $vet]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $vet)
    {
        Formation::delete($id, $vet);
        return redirect()->action('FormationController@index');
    }

    public function formation_404(){
        return view('formation.404');
    }
}
