<?php

namespace App\Http\Controllers;

use App\Models\IndividuGroupe;
use Illuminate\Http\Request;

class IndividuGroupeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedIGData = $request->validate([
            'fid_groupe' => 'required|exists:groupe,id_groupe',
            'id_individus' => 'required|array|exists:individu,id_individu'
        ]);

        foreach ($validatedIGData['id_individus'] as $idIndividu) {
            IndividuGroupe::create([
                'fid_groupe' => $validatedIGData['fid_groupe'],
                'fid_individu' => $idIndividu
            ]);
        }

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idIndividu, $idGroupe)
    {
        IndividuGroupe::delete($idIndividu, $idGroupe);

        return back();
    }
}
