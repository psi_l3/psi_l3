<?php

namespace App\Http\Middleware;

use App\Models\Composante;
use Closure;

class CheckComposanteExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route()->parameters()['composante'];
        if(!Composante::get($id)){
            return redirect('404/composante');
        }
        return $next($request);
    }
}
