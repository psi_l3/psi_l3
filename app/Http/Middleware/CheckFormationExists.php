<?php

namespace App\Http\Middleware;

use App\Models\Formation;
use Closure;

class CheckFormationExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route()->parameters()['formation'];
        $vet = $request->route()->parameters()['vet'];
        if(!Formation::get($id, $vet)){
            return redirect('404/formation');
        }
        return $next($request);
    }
}
