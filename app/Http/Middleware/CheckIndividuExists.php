<?php

namespace App\Http\Middleware;

use App\Models\Individu;
use Closure;

class CheckIndividuExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route()->parameters()['individu'];
        if(!Individu::get($id)){
            return redirect('404/individu');
        }
        return $next($request);
    }
}
