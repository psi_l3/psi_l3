<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class IndividuTypeIndividu
{

    const TABLE = 'individu_typeindividu';

    public static function exists($idIndividu, $idType){
        return DB::table(self::TABLE)
            ->where('fid_individu', $idIndividu)
            ->where('fid_type_individu', $idType)
            ->exists();
    }

    public static function get($idIndividu, $idType){
        return DB::table(self::TABLE)
            ->where('fid_individu', $idIndividu)
            ->where('fid_type_individu', $idType)
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)
            ->orderBy('libelle_type_individu', 'asc')
            ->get();
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($idIndividu, $idType, $data){
        $id = DB::table(self::TABLE)
            ->where('fid_individu', $idIndividu)
            ->where('fid_type_individu', $idType)
            ->update($data);
        return $id;
    }

    public static function delete($idIndividu, $idType){
        DB::table(self::TABLE)
            ->where('fid_individu', $idIndividu)
            ->where('fid_type_individu', $idType)
            ->delete();
    }

}
