<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class SeanceGroupe
{

    const TABLE = 'seance_groupe';

    public static function exists($idSeance, $idGroupe){
        return DB::table(self::TABLE)
            ->where('fid_seance', $idSeance)
            ->where('fid_groupe', $idGroupe)
            ->exists();
    }

    public static function get($idSeance, $idGroupe){
        return DB::table(self::TABLE)
            ->where('fid_seance', $idSeance)
            ->where('fid_groupe', $idGroupe)
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)->get();
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($idSeance, $idGroupe, $data){
        $id = DB::table(self::TABLE)
            ->where('fid_seance', $idSeance)
            ->where('fid_groupe', $idGroupe)
            ->update($data);
        return $id;
    }

    public static function delete($idSeance){
        DB::table(self::TABLE)
            ->where('fid_seance', $idSeance)
            ->delete();
    }

}
