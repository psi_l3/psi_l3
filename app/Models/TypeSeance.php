<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class TypeSeance
{

    const TABLE = 'type_seance';

    public static function exists($id){
        return DB::table(self::TABLE)
            ->where('id_type_seance', $id)
            ->exists();
    }

    public static function get($id){
        return DB::table(self::TABLE)
            ->where('id_type_seance', $id)
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)
            ->orderBy('libelle_type_seance', 'asc')
            ->get();
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table(self::TABLE)
            ->where('id_type_seance', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table(self::TABLE)
            ->where('id_type_seance', $id)
            ->delete();
    }

}
