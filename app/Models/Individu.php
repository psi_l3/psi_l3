<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Individu //extends Model
{

    const TABLE = "individu";


    public static function get($id){
        return DB::table('individu')->where('id_individu', $id)->first();
    }

    public static function getAll(){
        return DB::table('individu')->get();
    }

    public static function getAllWithTypes(){
        $individus = self::getAll();
        foreach ($individus as $individu) {
            $individu->types = self::getTypeFor($individu->id_individu);
        }
        return $individus;
    }

    public static function getTypeFor($id){
        return DB::table(self::TABLE)
            ->where('id_individu', $id)
            ->leftJoin('individu_typeindividu', 'individu_typeindividu.fid_individu', 'individu.id_individu')
            ->leftJoin('type_individu', 'type_individu.id_type_individu', 'individu_typeindividu.fid_type_individu')
            ->select('id_type_individu', 'libelle_type_individu')
            ->distinct()
            ->get();
    }

    public static function getEtudiants(){
        //Ajouter un where et une jointure sur type_individu pour récuperer que les étudiants
        return DB::table(self::TABLE)
        ->orderBy('nom_individu', 'asc')
        ->orderBy('prenom_individu', 'asc')
        ->get();
    }

    public static function getEnseignants(){
        //Ajouter un where et une jointure sur type_individu pour récuperer que les enseignants
        return DB::table(self::TABLE)
        ->orderBy('nom_individu', 'asc')
        ->orderBy('prenom_individu', 'asc')
        ->get();
    }

    //Récupère les séances d'un groupes
    public static function getSeanceFor($idIndividu){
        return DB::table(self::TABLE)
            ->where('id_individu', $idIndividu)
            ->leftJoin('seance', 'individu.id_individu', 'seance.fid_individu')
            ->leftJoin('seance_groupe', 'seance_groupe.fid_seance', 'seance.id_seance')
            ->leftJoin('groupe', 'seance_groupe.fid_groupe', 'groupe.id_groupe')
            ->leftJoin('type_seance', 'type_seance.id_type_seance', 'seance.fid_type_seance')
            ->leftJoin('salle', 'salle.id_salle', 'seance.fid_salle')
            ->select(
                'id_seance',
                'date_debut_seance',
                'date_fin_seance',
                'libelle_groupe',
                'nom_individu',
                'prenom_individu',
                'batiment',
                'numero_salle',
                'libelle_type_seance',
                'capacite',
                'nb_postes',
                'projecteur'
            )
            ->get();
    }


    public static function create($data){
        $id = DB::table('individu')->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table('individu')
            ->where('id_individu', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table('individu')->where('id_individu', $id)->delete();
    }

    public static function getIndividuData(){
        $data=DB::table('individu')
              ->select('id_individu','nom_individu','prenom_individu','mail_individu','tel_individu')
              ->get();
        return $data;
    }

    public static function exists($id){
        return DB::table(self::TABLE)
            ->where('id_individu', $id)
            ->exists();
    }
}
