<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Seance
{

    const TABLE = 'seance';

    public static function exists($id){
        return DB::table(self::TABLE)
            ->where('id_seance', $id)
            ->exists();
    }

    public static function get($id){
        return DB::table(self::TABLE)
            ->where('id_seance', $id)
            ->first();
    }

    public static function getWithGroupe($id){
        return DB::table(self::TABLE)
            ->where('id_seance', $id)
            ->leftJoin('seance_groupe', 'seance_groupe.fid_seance', 'seance.id_seance')
            ->leftJoin('groupe', 'seance_groupe.fid_groupe', 'groupe.id_groupe')
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)
            ->orderBy('date_debut_seance', 'desc')
            ->get();
    }

    public static function getAllWithGroupe(){
        return DB::table(self::TABLE)
            ->leftJoin('seance_groupe', 'seance_groupe.fid_seance', 'seance.id_seance')
            ->leftJoin('groupe', 'seance_groupe.fid_groupe', 'groupe.id_groupe')
            ->leftJoin('individu', 'seance.fid_individu', 'individu.id_individu')
            ->orderBy('date_debut_seance', 'desc')
            ->get();
    }

    public static function joinedGet($id){
        return DB::table(self::TABLE)
            ->where('id_seance', $id)
            ->leftJoin('type_seance', 'type_seance.id_type_seance', 'seance.fid_type_seance')
            ->leftJoin('individu', 'individu.id_individu', 'seance?fid_individu')
            ->leftJoin('salle', 'salle.id_salle', 'seance.fid_salle')
            ->select(
                'id_seance',
                'id_type_seance',
                'libelle_type_seance',
                'id_individu',
                'nom_individu',
                'prenom_individu',
                'id_salle',
                'batiment',
                'numero_salle',
                'capacite',
                'nb_postes',
                'projecteur'
            );
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table(self::TABLE)
            ->where('id_seance', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table(self::TABLE)
            ->where('id_seance', $id)
            ->delete();
    }

}
