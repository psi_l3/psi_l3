<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class FormationComposante
{

    const TABLE = 'formation_composante';

    public static function getAll(){
        return DB::table('formation_composante')->get();
    }

    // public static function get($idF, $idC){
    //     return DB::table('formation')->where('id_formation', $id)->first();
    // }

    public static function create($idFormation, $idComposante, $vet = 1){
        $id = DB::table('formation_composante')->insertGetId([
            'fid_formation' => $idFormation,
            'f_vet' => $vet,
            'fid_composante' => $idComposante
        ]);
        return $id;
    }


    public static function getByFormation($fid_formation){
        return DB::table(self::TABLE)->where('fid_formation', $fid_formation)->get();
    }

    public static function update($idFormation, $vet, $idComposantes){
        DB::table(self::TABLE)
            ->where('fid_formation', $idFormation)
            ->where('f_vet', $vet)
            ->whereNotIn('fid_composante', $idComposantes)
            ->delete();

        foreach ($idComposantes as $idComposante) {
            DB::table(self::TABLE)->insertOrIgnore([
                'fid_formation' => $idFormation,
                'f_vet' => $vet,
                'fid_composante' => $idComposante
            ]);
        }
    }

    public static function prepare_join(){
        return DB::table(self::TABLE)
            ->join('formation', function($join){
                $join->on('formation_composante.fid_formation', 'formation.id_formation')
                    ->on('formation_composante.f_vet', 'formation.vet');
            })
            ->join('composante', 'formation_composante.fid_composante', 'composante.id_composante');
    }
}
