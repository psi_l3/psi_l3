<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Salle
{

    const TABLE = 'salle';

    public static function exists($id){
        return DB::table(self::TABLE)
            ->where('id_salle', $id)
            ->exists();
    }

    public static function get($id){
        return DB::table(self::TABLE)
            ->where('id_salle', $id)
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)
            ->orderBy('id_salle', 'asc')
            ->get();
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table(self::TABLE)
            ->where('id_salle', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table(self::TABLE)
            ->where('id_salle', $id)
            ->delete();
    }

}
