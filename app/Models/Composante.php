<?php

namespace App\Models;

use App\Models\FormationComposante;
use Illuminate\Support\Facades\DB;

class Composante
{

    const TABLE = 'composante';

    public static function getAll(){
        return DB::table(self::TABLE)
        ->orderBy('libelle_composante', 'asc')
        ->get();
    }

    public static function get($id){
        return DB::table(self::TABLE)->where('id_composante', $id)->first();
    }

    public static function store($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table(self::TABLE)
            ->where('id_composante', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table(self::TABLE)->where('id_composante', $id)->delete();
    }

    public static function getFormations($id){
        $query = FormationComposante::prepare_join();
        return $query
            ->where('fid_composante', $id)
            ->select('fid_formation', 'libelle_formation', 'vet')
            ->orderBy('libelle_formation', 'asc')
            ->get();
    }

    public static function getWithFormations($id){
        $composante = self::get($id);
        $composante->formations = self::getFormations($id);

        return $composante;
    }

}
