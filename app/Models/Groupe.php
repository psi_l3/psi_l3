<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Groupe
{
    public static function getGroupeData(){
        return DB::table('groupe')
                  ->select('id_groupe','fid_niveau','fid_formation','fid_modalite','libelle_groupe','annee')
                  ->distinct()
                  ->get();
    }
    public static function getGroupes(){
      return DB::table('groupe')
                ->distinct()
                ->get(['libelle_groupe']);
    }

    const TABLE = 'groupe';

    public static function exists($id){
        return DB::table(self::TABLE)
            ->where('id_groupe', $id)
            ->exists();
    }

    public static function get($id){
        return DB::table(self::TABLE)
            ->where('id_groupe', $id)
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)
            ->orderBy('libelle_groupe', 'asc')
            ->get();
    }

    //récupère les étudiants du groupe avec l'id $id
    public static function getEtudiants($id){
        return DB::table(self::TABLE)
            ->where('id_groupe', $id)
            ->join('individu_groupe', 'individu_groupe.fid_groupe', 'groupe.id_groupe')
            ->join('individu', 'individu_groupe.fid_individu', 'individu.id_individu')
            ->select('id_individu', 'id_groupe', 'nom_individu', 'prenom_individu')
            ->orderBy('nom_individu', 'asc')
            ->orderBy('prenom_individu', 'asc')
            ->get();
    }

    //Récupère les séances d'un groupes
    public static function getSeanceFor($idGroupe){
        return DB::table(self::TABLE)
            ->where('id_groupe', $idGroupe)
            ->leftJoin('seance_groupe', 'seance_groupe.fid_groupe', 'groupe.id_groupe')
            ->leftJoin('seance', 'seance_groupe.fid_seance', 'seance.id_seance')
            ->leftJoin('type_seance', 'type_seance.id_type_seance', 'seance.fid_type_seance')
            ->leftJoin('individu', 'individu.id_individu', 'seance.fid_individu')
            ->leftJoin('salle', 'salle.id_salle', 'seance.fid_salle')
            ->select(
                'id_seance',
                'date_debut_seance',
                'date_fin_seance',
                'libelle_groupe',
                'nom_individu',
                'prenom_individu',
                'batiment',
                'numero_salle',
                'libelle_type_seance',
                'capacite',
                'nb_postes',
                'projecteur'
            )
            ->get();
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table(self::TABLE)
            ->where('id_groupe', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table(self::TABLE)
            ->where('id_groupe', $id)
            ->delete();
    }

}
