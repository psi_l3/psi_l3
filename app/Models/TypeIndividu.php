<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class TypeIndividu
{

    const TABLE = 'type_individu';

    public static function exists($id){
        return DB::table(self::TABLE)
            ->where('id_type_individu', $id)
            ->exists();
    }

    public static function get($id){
        return DB::table(self::TABLE)
            ->where('id_type_individu', $id)
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)
            ->orderBy('libelle_type_individu', 'asc')
            ->get();
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table(self::TABLE)
            ->where('id_type_individu', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table(self::TABLE)
            ->where('id_type_individu', $id)
            ->delete();
    }

}
