<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class IndividuGroupe
{

    const TABLE = 'individu_groupe';


    public static function create($data){
        DB::table(self::TABLE)->insertOrIgnore($data);
    }

    public static function delete($idIndividu, $idGroupe){
        DB::table(self::TABLE)
            ->where('fid_individu', $idIndividu)
            ->where('fid_groupe', $idGroupe)
            ->delete();
    }

}
