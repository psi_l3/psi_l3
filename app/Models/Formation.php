<?php

namespace App\Models;

use App\Models\FormationComposante;
use Illuminate\Support\Facades\DB;

class Formation
{

    const TABLE = 'formation';

    public static function get($id, $vet){
        return DB::table('formation')
            ->where('id_formation', $id)
            ->where('vet', $vet)
            ->first();
    }

    public static function getAll(){
        return DB::table('formation')
            ->orderBy('libelle_formation', 'asc')
            ->get();
    }

    public static function exists($id, $vet){
        return DB::table(self::TABLE)
            ->where('id_formation', $id)
            ->where('vet', $vet)
            ->exists();
    }

    /**
    * Récupère toutes le versions d'une formation
    **/
    public static function getFormationAllVersion($id){
        return DB::table('formation')
            ->where('id_formation', $id)
            ->orderBy('vet', 'asc')
            ->get();
    }

    //Create
    public static function create($data){
        $id = DB::table('formation')->insertGetId($data);
        return $id;
    }

    public static function update($id, $vet, $data){
        $id = DB::table('formation')
            ->where('id_formation', $id)
            ->where('vet', $vet)
            ->update($data);
        return $id;
    }

    public static function delete($id, $vet){
        DB::table('formation')
            ->where('id_formation', $id)
            ->where('vet', $vet)
            ->delete();
    }

    protected static function getComposantes($id, $vet){
        return DB::table(self::TABLE)
            ->join('formation_composante', 'formation_composante.fid_formation', 'formation.id_formation')
            ->join('composante', 'formation_composante.fid_composante', 'composante.id_composante')
            ->where('fid_formation', $id)
            ->where('vet', $vet)
            ->where('f_vet', $vet)
            ->select('fid_composante', 'libelle_composante')
            ->orderBy('libelle_composante', 'asc')
            ->get();
    }

    public static function getWithComposantes($id, $vet){
        $formation = self::get($id, $vet);
        $formation->composantes = self::getComposantes($id, $vet);
        return $formation;
    }

    public static function getAllWithComposantes(){
        $formations = Formation::getAll();

        foreach ($formations as $formation){
            $formation->composantes = self::getComposantes($formation->id_formation, $formation->vet);
        }
        return $formations;
    }

    public static function isLatestVersion($id, $vet){
        return DB::table(self::TABLE)
            ->where('id_formation', $id)
            ->where('vet', '>', $vet)
            ->doesntExist();
    }
}
