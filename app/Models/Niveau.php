<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Niveau
{

    const TABLE = 'niveau';

    public static function get($id){
        return DB::table(self::TABLE)
            ->where('id_niveau', $id)
            ->first();
    }

    public static function getAll(){
        return DB::table(self::TABLE)->get();
    }

    public static function create($data){
        $id = DB::table(self::TABLE)->insertGetId($data);
        return $id;
    }

    public static function update($id, $data){
        $id = DB::table(self::TABLE)
            ->where('id_niveau', $id)
            ->update($data);
        return $id;
    }

    public static function delete($id){
        DB::table(self::TABLE)
            ->where('id_niveau', $id)
            ->delete();
    }

}
