<?php

namespace App\Models;

use App\Models\Individu;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;

class ImportIndividu implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       $dataIndividu=[
         'id_individu' => $row['id_individu'],
         'arpeg' => $row['arpeg'],
         'nom_individu' => $row['nom_individu'],
         'prenom_individu' => $row['prenom_individu'],
         'mail_individu' =>$row['mail_individu'],
         'tel_individu' => $row['tel_individu'],
       ];
       DB::table('individu')->updateOrInsert((array)$dataIndividu);
       if($row['type_individu']=='Etudiant' | $row['type_individu']=='Etudiante') {
         $id=1;
       }
       else if($row['type_individu']=='Chargé de TD' | $row['type_individu']=='Chargée de TD') {
         $id=2;;
       }
       else if($row['type_individu']=='Maître de conférence') {
         $id=3;
       }
       else if($row['type_individu']=='Professeur' | $row['type_individu']=='Professeure') {
         $id=4;
       }

      $dataTypeIndividuAnnee=[
        'fid_individu'=>$row['id_individu'],
        'fid_type_individu' =>$id,
        'annee'=>$row['annee'],
      ];
      DB::table('individu_typeindividu')->updateOrInsert((array)$dataTypeIndividuAnnee);
   }
}
