<?php
namespace App\Export;
use App\Models\Individu;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class exportData implements FromCollection, WithHeadings{

  public function collection(){
    $resultat=Individu::getIndividuData();
    return $resultat;
  }

  public function headings():array{
    return[
      'id_individu',
      'nom_individu',
      'prenom_individu',
      'mail_individu',
      'tel_individu',
    ];
  }

}
?>
