<?php
namespace App\Export;
use App\Models\Groupe;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpParser\Error;

class exportGroupe implements FromCollection, WithHeadings{

  public function collection(){
    $resultat=Groupe::getGroupeData();
    $resultat=DB::table('groupe')
              ->select('libelle_groupe','individu.id_individu','individu.nom_individu','individu.prenom_individu','niveau.libelle_niveau','formation.libelle_formation','formation.vet','modalite.libelle_modalite')//, 'missions.thematique', 'entreprises.nom as nomEntreprise')
              ->leftJoin('individu_groupe','groupe.id_groupe','=','individu_groupe.fid_groupe')
              ->leftJoin('individu','individu.id_individu','=','individu_groupe.fid_individu')
              ->leftJoin('niveau','groupe.fid_niveau','=','niveau.id_niveau')
              ->leftJoin('formation','formation.id_formation','=','groupe.fid_formation')
              ->leftJoin('modalite','groupe.fid_modalite','=','modalite.id_modalite')
              ->where('libelle_groupe',request()->input('libelleGroupeChoix'))
              ->get();
    return $resultat;
  }

  public function headings():array{
    return[
      'groupe',
      'numero_individu',
      'nom_individu',
      'prenom_individu',
      'niveau',
      'formation',
      'vet',
      'modalite'
    ];
  }
}
?>
